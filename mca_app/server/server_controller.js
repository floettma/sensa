Meteor.publish("models", function () {
    return Models.find({owner: this.userId});
});

Meteor.publish("analysis", function () {
    return Analysis.find({owner: this.userId});
});


var createAnalysis = function (model) {
    if (Models.findOne({_id: model.id})) {
        Analysis.insert({model_id: model.id,
                         owner: model.owner,
                         get_model_details: {name: "Details", priority: 1},
                         get_steady_state: {name: "Steady state", priority: 2},
                         get_parameter_elasticities: {name: "P-Elasticities", priority: 3},
                         get_metabolite_elasticities: {name: "E-Elasticities", priority: 4},
                         get_flux_cc: {name: "Flux control coefficients", priority: 5},
                         get_conc_cc: {name: "Concentration control coefficients", priority: 6},
                         get_flux_rc: {name: "Flux response coefficients", priority: 7},
                         get_conc_rc: {name: "Concentration response coefficients", priority: 8},
                         get_time_varying_conc_rc: {name: "Time dependent response coefficients", priority: 9}
                        });
        return true;
    }
    return false;
};

Meteor.methods({
    createModel: function (options) {
        if (options.name.length > 100)
            throw new Meteor.Error(413, "Name too long");
        if (! this.userId)
            throw new Meteor.Error(403, "You must be logged in");

        var id = options._id || Random.id();
        Models.insert({
            _id: id,
            owner: this.userId,
            name: options.name,
            file: options.file
        });
        createAnalysis({id: id, owner: this.userId});
        return id;
    },
    removeModel: function (options) {
        if (!Models.findOne({_id: options.id},{fields: {owner: 1}})['owner'] === this.userId)
            throw new Meteor.Error(403, "Access Denied");
        Models.remove({_id: options.id});
        Analysis.remove({model_id: options.id});
    },
    //start a python process to calculate stuff for the current model
    start_MCA: function(current_model) {
        var childProcess = Npm.require('child_process');
        var path = Npm.require('path');

        mca[current_model] = childProcess.spawn('python', ['assets/app/api.py'], {env: process.env});

        console.log('test running');
        mca[current_model].stdin.setEncoding = 'utf-8';

        // listen to data from python
        mca[current_model].stdout.on('data', function (d) {
            console.log('python out: ' + d);
        });

        mca[current_model].stderr.on('data', function(d) {
            console.log('python: ' + d);
        });
    },
    // call python with method name
    mca: function(current_model, method, arguments) {
        var command = {method: method,
                       model: current_model,
                       arguments: arguments};
        mca[current_model].stdin.write(JSON.stringify(command) + '\n');
        return command;
    },
    quit_mca: function(current_model) {
        mca[current_model].stdin.write('quit\n');
    },

    // send an email to us
    sendEmail: function (to, from, subject, text) {
        check([to, from, subject, text], [String]);

        // Let other method calls from the same client start running,
        // without waiting for the email sending to complete.
        this.unblock();

        var senderAdress = from == "" ? "sensa@hu-berlin.de": from;

        Email.send({
          to: ['max.floettmann@biologie.hu-berlin.de','thomas.spiesser@biologie.hu-berlin.de'],
          from: senderAdress,
          subject: subject,
          text: text
        });
    }
});