Meteor.subscribe("models");
Meteor.subscribe("analysis");

//select a model on startup if one exists
Meteor.startup(function () {


    $(document).ready(function() {    
        //g analytics
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-54853745-1', 'auto');
        ga('send', 'pageview');

    });

    Session.set("normalization", false);
    Session.set("selected_analysis", "get_model_details");
    // define global variable called, that gets an object per model
    // id. this object stores flags if an analysis was already called
    // or not. this prevents calling the same analysis many times.
    called = {};
    Deps.autorun(function () {
        var selected_model = Session.get("selected_model");

        if (! selected_model || ! Models.findOne(selected_model)) {
            var model = Models.findOne();
            if (model) {
                Session.set("selected_model", model._id);
                called[model._id] = {};
            } else
            Session.set("selected_model", null);
        }

        else {
            var ana_list = Analysis.findOne({model_id: selected_model},
                                            {fields: {model_id: 0,
                                                      owner: 0}});
            // start a new process if necessary
            if(!mca.hasOwnProperty(selected_model)){
                mca[selected_model] = Meteor.call('start_MCA', selected_model);
            }
            // for all types of analysis
            for (var key in ana_list) {
                var call_now = false;
                if(called.hasOwnProperty(selected_model)) {
                    call_now = !called[selected_model].hasOwnProperty(key); //call_now is true when the analysis was not called yet
                }
                if (!ana_list[key].hasOwnProperty('data') && call_now){
                    // if there is no result yet and the call hasn't been made before
                    Meteor.call('mca', selected_model, key);
                    //compute it
                    called[selected_model][key] = 1;
                    //remember that the computation call was made.
                }
            }
        };
    });
});

Template.alerterror.error_object = function () {
    var selected_model = Session.get('selected_model');
    var selected_analysis = Session.get('selected_analysis');
    var fields =  {};
    fields[selected_analysis] = 1;
    try {
        var ana_object = Analysis.findOne({model_id: selected_model},
                                          {fields: fields});
        ana_object = ana_object[selected_analysis];
        var error_object = [];

        if (ana_object.hasOwnProperty('error')) {
            for (var i=0; i<ana_object.error.length; i++) {
                error_object.push(
                    {error_message: ana_object.error[i][0],
                     error_critical: ana_object.error[i][1]}
                );
            }
        }
        else {
            error_object = false;
        };
        return error_object;
    }
    catch(e) {
        return [];
    };

};

Template.alertwarning.warning_object = function () {
    var selected_model = Session.get('selected_model');
    var selected_analysis = Session.get('selected_analysis');
    var fields =  {};
    fields[selected_analysis] = 1;
    try {
        var ana_object = Analysis.findOne({model_id: selected_model});
                                          //{fields: fields});
        ana_object = ana_object[selected_analysis];
        var warning_object = [];

        if (ana_object.hasOwnProperty('warning')) {
            for (var i=0; i<ana_object.warning.length; i++) {
                warning_object.push(
                    {
                        warning_message: ana_object.warning[i]
                    });
            }
        }

        return warning_object;
    }
    catch(e) {
        return [];
    };
};

Template.alertwarning.no_of_warnings = function () {
    var selected_model = Session.get('selected_model');
    var selected_analysis = Session.get('selected_analysis');
//    var fields =  {_id: 0};
    fields[selected_analysis] = 1;
    try {
        var ana_object = Analysis.findOne({model_id: selected_model});
                                          //{fields: fields});
        ana_object = ana_object[selected_analysis];
        var warning_object = [];

        if (ana_object.hasOwnProperty('warning')) {
            no_of_warnings = ana_object.warning.length;
        }

        return no_of_warnings;
    }
    catch(e) {
        return 0;
    }
};

Template.models.model = function () {
    return Models.find({}, {fields: {name: 1}});
};


Template.my_model_details.reaction_details = function() {
    var selected_model = Session.get('selected_model');
    var selected_analysis = "get_model_details";
    var fields =  {};
    fields[selected_analysis] = 1;
    var model_details = Analysis.findOne({model_id: selected_model},
                                         {fields: fields});
    var reaction_details = [];
    if(typeof(model_details) !== "undefined" && 
       model_details.hasOwnProperty(selected_analysis) && 
       model_details[selected_analysis].hasOwnProperty('reaction_ids')) {
        for (var i=0; i<model_details[selected_analysis].reaction_ids.length; i++) {
            reaction_details[i] = {
                id: model_details[selected_analysis].reaction_ids[i],
                name: model_details[selected_analysis].reaction_names[i],
                substrate: model_details[selected_analysis].reaction_substrates[i],
                product:  model_details[selected_analysis].reaction_products[i],
                modifier:  model_details[selected_analysis].reaction_modifiers[i],
                kinetic:  model_details[selected_analysis].reaction_k[i]
            };
        }
    }
    return reaction_details
};


Template.my_model_details.parameter_details = function() {
    var selected_model = Session.get('selected_model');
    var selected_analysis = "get_model_details";
    var fields =  {};
    fields[selected_analysis] = 1;
    var model_details = Analysis.findOne({model_id: selected_model},
                                         {fields: fields});
    var parameter_details = [];
    if(typeof(model_details) !== "undefined" && 
       model_details.hasOwnProperty(selected_analysis) && 
       model_details[selected_analysis].hasOwnProperty('parameter_ids')) {
        for (var i=0; i<model_details[selected_analysis].parameter_ids.length; i++) {
            parameter_details[i] = {
                id: model_details[selected_analysis].parameter_ids[i],
                name: model_details[selected_analysis].parameter_names[i],
                value: model_details[selected_analysis].parameter_values[i]
            };
        }
    }
    return parameter_details
};


Template.my_model_details.species_details = function() {
    var selected_model = Session.get('selected_model');
    var selected_analysis = "get_model_details";
    var fields =  {};
    fields[selected_analysis] = 1;
    var model_details = Analysis.findOne({model_id: selected_model},
                                         {fields: fields});
    var species_details = [];
    if(typeof(model_details) !== "undefined" && 
       model_details.hasOwnProperty(selected_analysis) && 
       model_details[selected_analysis].hasOwnProperty('species_ids')) {
        for (var i=0; i<model_details[selected_analysis].species_ids.length; i++) {
            species_details[i] = {
                id: model_details[selected_analysis].species_ids[i],
                name: model_details[selected_analysis].species_names[i],
                init_conc: model_details[selected_analysis].species_init_conc[i]
            };
        }
    }
    return species_details
};


Template.nav_bar.events({
    'change .upload': function(e, template) {
        var target = e.target || e.srcElement; //different depending on browser
        _.each(target.files, function(file) {
            var reader = new FileReader();
            reader.onload = function(e) {
                // Add it to the db
                Meteor.call("createModel",
                            {name: file.name,
                             file: e.target.result,
                             analysis: {}}, 
                            function(e, result){
                                // select model on upload
                                Session.set("selected_model", result);
                                called[result] = {};
                                Session.set("selected_analysis", 'get_model_details');
                            });
            }
            reader.readAsText(file);
        })
    }
});

Template.export_data.events({
    'click #custom_export': function (event, template) {
        var selected_analysis = Session.get('selected_analysis');
        var fields =  {};
        fields[selected_analysis] = 1;
        var current_analysis = Analysis.findOne({model_id: Session.get('selected_model')},
                                                {fields: fields});
        var data = current_analysis[selected_analysis].data;
        var x_axis, y_axis;
        if(current_analysis[selected_analysis].x_axis_alt != undefined) {
            x_axis = current_analysis[selected_analysis].x_axis_alt;
        }
        else{
            x_axis = current_analysis[selected_analysis].x_axis;
        }
        if(current_analysis[selected_analysis].y_axis_alt != undefined) {
            y_axis = current_analysis[selected_analysis].y_axis_alt;
        }
        else{
            y_axis = current_analysis[selected_analysis].y_axis;
        }

        var csvContent = "";
        // timecourses have an extra dimension, so they mus be handled differently
        var time;
        if (_.contains(["get_time_varying_conc_rc"],selected_analysis)) {
            time = current_analysis[selected_analysis].time;
            var labels = _.map(x_axis, function(x) {return _.map(y_axis, function(y) {return x + "_" + y})});
            labels = _.flatten(labels);
            csvContent += "time," + time.join(",") + "\n"; //print headers
            data.forEach(function(infoArray, index) {
                var dataString = infoArray.join(",");
                dataString = labels[index] + "," + dataString;
                csvContent += dataString + "\n"//index < infoArray.length ? dataString + "\n" : dataString;
            });
        }
        else if (_.contains(["get_model_details"],selected_analysis)) {
            time = current_analysis[selected_analysis].time;
            csvContent += "time," + time.join(",") + "\n"; //print headers
            data.forEach(function(infoArray, index) {
                var dataString = infoArray.join(",");
                dataString = x_axis[index] + "," + dataString;
                csvContent += dataString + "\n"//index < infoArray.length ? dataString + "\n" : dataString;
            });
        }
        else {
            csvContent += "," + y_axis.join(",") + "\n"; //print headers
            data.forEach(function(infoArray, index){
                var dataString = infoArray.join(",");
                dataString = x_axis[index] + "," + dataString;
                csvContent += index < infoArray.length ? dataString + "\n" : dataString;

            });
        };
        // make filename
        var model = Models.findOne({_id: Session.get("selected_model")},
                                   {fields: {name: 1}});
        if (typeof(model) !== "undefined") {
            var name = model.name
            var filename;
            filename = name.substring(0,name.length-4)
                + "_"
                + Session.get("selected_analysis")
                + ".csv";
        }
        var csvBlob = new Blob([csvContent], {type: "text/plain;charset=utf-8"});
        saveAs(csvBlob, filename);
    }
});


Template.model_name.selected = function() {
    return Session.equals("selected_model", this._id) ? "active" : '';
};


Template.model_name.events({
    'click': function (event) {
        Session.set("selected_model", this._id);
        called[this._id] = {};
        Session.set("selected_analysis", 'get_model_details');
    },
    'click i': function() {
        Meteor.call('removeModel', {id: this._id});
        Session.set("selected_model", null);
        // Analysis.remove({model: this._id});
    }
});


Template.register.errorMessage = function() {
    return Session.get( "errorMessage" );
};


Template.register.events({
    'click #sign_up_button' : function(e, t) {
        e.preventDefault();
        var email = t.find('#account-email').value;
        var password = t.find('#account-password').value;
        // Trim and validate the input
        // trim helper
        var trimInput = function(val) {
            return val.replace(/^\s*|\s*$/g, "");
        }
        var email = trimInput(email);
        Accounts.createUser({email: email, password : password}, function(err){
            if (err) {
                Session.set( "errorMessage", "Sorry, "+err.reason );
                // Inform the user that account creation failed
            } else {
                Session.set( "errorMessage", '' );
                // Success. Account has been created and the user
                // has logged in successfully. 
            }
        });
        return false;
    }
});


Template.register.events({
    'click #log_in_button' : function(e, t) {
        e.preventDefault();
        var email = t.find('#account-email').value;
        var password = t.find('#account-password').value;
        // Trim and validate the input
        // trim helper
        var trimInput = function(val) {
            return val.replace(/^\s*|\s*$/g, "");
        }
        var email = trimInput(email);
        Meteor.loginWithPassword(email, password, function(err){
            if (err) {
                Session.set( "errorMessage", "Sorry, "+err.reason );
                // Inform the user that account creation failed
            } else {
                Session.set( "errorMessage", '' );
                // Success. The user has logged in successfully. 
            }
        });
        return false;
    }
});


Template.register.events({
    'click .trial_button': function (e) {
        $('#registerModal').modal('hide');
        e.preventDefault();
        var sId = Meteor.default_connection._lastSessionId;
        var test_email = 'test@' + sId + '.com';
        var pw = '111111';
        Accounts.createUser({email: test_email, password : pw, profile: {name: 'test'}}, function(err){
            if (err) {
                Meteor.loginWithPassword(test_email, pw, function(err){
                    if (err) {
                        Session.set( "errorMessage", "Sorry, "+err.reason );
                    }
                })
                // Inform the user that account creation failed
            } else {
                Session.set( "errorMessage", '' );
                // Success. Account has been created and the user
                // has logged in successfully. 
            }
        });
    }
});


Template.modalInner.events({
    'click input.btn': function (event, template) {
        Meteor.call('sendEmail',
                    '',
                    template.find('#emailSender').value,
                    template.find('#leSubject').value,
                    template.find('#emailMessage').value
                   );
        $('#myModal').modal('hide');
    }
}); 

Template.modalInner.helpers({
    'user': function(){
        return Meteor.userId() ? Meteor.user().emails[0].address : '';
    }
});


// function logRenders () {
//     _.each(Template, function (template, name) {
//       var oldRender = template.rendered;
//       var counter = 0;

//       template.rendered = function () {
//         console.log(name, "render count: ", ++counter);
//         oldRender && oldRender.apply(this, arguments);
//       };
//     });
//   }
// logRenders ();