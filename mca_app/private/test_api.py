#!/usr/bin/env python


import os, sys
#sys.path.append( '/Users/jannis/Documents/code/mca_webapp/sbml-mca' ) # local fix for jannis
import api



model_id = 'dontknow'
host     = 'localhost'



# port no. defined equal to fix as in api
if (os.uname()[1] == 'arthur'):
    port = 27017
else:
    port = 3002

# generate API object
a = api.api( model_id, host, port )


# if everything worked fine, is should give an error when loading the model:
# While loop terminatedERRORS: Could not parse SBML modelThere was a critical error, exiting!
# if no connection to DB: 
# pymongo.errors.ConnectionFailure: could not connect to localhost:3002: [Errno 61] Connection refused
