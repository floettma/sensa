#!/usr/bin/env python

"""
tentative API for MCA functions
"""

import libsbml, sys, os, numpy
from sbml_mca import sbml_mca, sbml_validator
import pymongo

if float(pymongo.version[0:3]) >= 2.4:
    import pymongo.mongo_client
elif float(pymongo.version[0:3]) >= 2.2:
    import pymongo.MongoClient
else:
    import pymongo.connection

class api:

    # standard error messages for the different functions
    std_error_messages = { 'get_model_details':            'An error occured on initialization.',
                           'get_steady_state':             'An error occured while finding a steady state.',
                           'get_parameter_elasticities':   'An error occured while computing parameter elasticities.',
                           'get_metabolite_elasticities':  'An error occured while computing parameter elasticities.',
                           'get_flux_cc':                  'An error occured while computing flux control coefficients.',
                           'get_conc_cc':                  'An error occured while computing concentration control coefficients.',
                           'get_flux_rc':                  'An error occured while computing flux response coefficients.',
                           'get_conc_rc':                  'An error occured while computing concentration response coefficients.',
                           'get_time_varying_conc_rc':     'An error occured while computing time varying concentration response coefficients.' }


    def __init__( self, model_id, host, port):
        """ Constructor for api class
            @param model_id: model id in database
            @param host: mongodb hostname
            @param port: mongodb port
            @type model_id: string
            @type host: string
            @type port: int
        """
        # set up mongo connection
        if float(pymongo.version[0:3])>=2.4:
            client = pymongo.mongo_client.MongoClient(host, port)
        if float(pymongo.version[0:3])>=2.2:
            client = pymongo.MongoClient(host, port)
        else:
            client = pymongo.connection.Connection(host, port )

        self._db = client.meteor

        self._model_id = model_id

        # get SBML model from DB
        db_model = self._db.models.find_one( {'_id': model_id} )
        # print db_model

        try:
            doc = libsbml.readSBMLFromString( str( db_model['file'].encode('utf-8') ) )
            model = doc.getModel()
        except Exception, e:
            self._write_error( 'Could not parse SBML model',
                               'get_model_details',
                               critical=True)

        # validate SBML model
        validator = sbml_validator.sbml_validator( model )
        [warnings,errors] = validator.validate()

        #sys.stderr.write( str(warnings) )
        if warnings!=[]:
            self._db.analysis.update( {'model_id': self._model_id},
                                      {'$set': {'get_model_details.warning': warnings, 'get_model_details.data': [] }}, upsert=True  )


        if errors!=[]: # if there were errors, write them in DB and terminate computations
            self._db.analysis.update( {'model_id': self._model_id},
                                      {'$set': {'get_model_details.error': [(e,True) for e in errors], 'get_model_details.data': [] }}, upsert=True  )
            sys.stderr.write('There was a critical error, exiting!')
            sys.exit()

        # construct sbml_mca object and raise critical error in case of failure
        try:
            self._sbml_mca = sbml_mca.sbml_mca( model )
        except  Exception, e:
            sys.stderr.write( str(type(e)) + ': ' + e.message )
            self._write_error( 'An error occured on initialization.',
                               'get_model_details',
                               critical=True )
    def _id( self ):
        """ dummy function. has no purpose other than being defined. """
        return None

    def _write_error( self, error, f_name, critical=True):
        """ writes error in database, exits script if error is critical
        @param error: error message
        @param f_name: name of the function that calls this method (for database insertion)
        @param critical: boolean indicating if error is critical
        @type error: string
        @type f_name: string
        @type critical: boolean
        """
        self._db.analysis.update( {'model_id': self._model_id},
                                  {'$set': {f_name+'.error': [(error,critical)], f_name+'.data': []} }, upsert=True  )
        #result = self._make_result_dict( f_name, [1], [1], [1], [1] )
        #result[f_name+'.error'] = [(error,critical)]
        #sys.stderr.write( str((error,critical)) )
        #self._update_db( result )
        sys.stderr.write('ERRORS: ' + str(error))
        if critical:
            sys.stderr.write('There was a critical error, exiting!')
            sys.exit()

    def _handle_error( self, error, f_name ):
        """ error handling function.
        there are 3 classes of errors: (i) Critical_errors: computations are stopped, (ii) Noncritical_errors: stopping is not necessary
        and (iii) unkown errors: give back standard error message (these are errors that could not be forseen
        @param error: error
        @param f_name: name of function calling """
        sys.stderr.write( str(type(error)) + ': ' + error.message )
        if type(error)==type(sbml_mca.Critical_error()):
            msg = error.message
            critical = True
        elif type(error)==type(sbml_mca.Noncritical_error()):
            msg = error.message
            critical = False

            # treat normalization errors as warnings, even though they are actually errors
            if msg == 'Error: Normalization failed. Value to divide by is too small.':
                warnings = ['Warning: Normalization failed. Division by zero encountered. Did not return normalized matrix.']
                # write normalization error as warning in DB
                self._db.analysis.update( {'model_id': self._model_id},
                                          {'$set': {f_name+'.warning': warnings } }, upsert=True  )
                return # stop further error treatment
        else:
            msg = self.std_error_messages[f_name]
            critical = False
        self._write_error( msg, f_name, critical )


    def _update_db( self, result ):
        """ method to write reults in database
        @type result: dictionary
        @param result: dictionary containing results to be writen in DB
        """
        return self._db.analysis.update( {'model_id': self._model_id},
                                  {'$set': result}, upsert=True )

    def _make_result_dict( self,
                           f_name,
                           data,
                           data_normalized=None,
                           x_axis=None,
                           x_axis_alt=None,
                           y_axis=None,
                           y_axis_alt=None,
                           z_axis=None,
                           z_axis_alt=None,
                           time=None):
        """ method to construct a result dictionary that can be sent to the DB
        @param data: data matrix/vector
        @param f_name: name of the calling function
        @param x_axis: label for x_axis
        @param y_axis: label for y_axis
        @param z_axis: label for z_axis
        """
        result = { f_name+'.data': data,
                   f_name+'.data_normalize': data_normalized,
                   f_name+'.x_axis': x_axis,
                   f_name+'.x_axis_alt': x_axis_alt,
                   f_name+'.y_axis': y_axis,
                   f_name+'.y_axis_alt': y_axis_alt,
                   f_name+'.z_axis': z_axis,
                   f_name+'.z_axis_alt': z_axis_alt,
                   f_name+'.time': time}
        return result

    def get_model_details( self ):
        """ get details of SBML model
        """
        result = {}
        try:
            result = {'get_model_details.model_id':              self._sbml_mca.get_model_id(),
                      'get_model_details.model_name':            self._sbml_mca.get_model_name(),
                      'get_model_details.species_ids':           self._sbml_mca.get_species_ids( include_constant=True ),
                      'get_model_details.species_ids_not_const': self._sbml_mca.get_species_ids( include_constant=False ),
                      'get_model_details.species_names':         self._sbml_mca.get_species_names( include_constant=True ),
                      'get_model_details.species_init_conc':     self._sbml_mca.get_species_initial_concentrations( include_constant=True ),
                      'get_model_details.species_init_amount':   self._sbml_mca.get_species_initial_amounts( include_constant=True ),
                      'get_model_details.parameter_ids':         self._sbml_mca.get_parameter_ids(),
                      'get_model_details.parameter_names':       self._sbml_mca.get_parameter_names(),
                      'get_model_details.parameter_values':      self._sbml_mca.get_parameter_values().tolist(),
                      'get_model_details.reaction_ids':          self._sbml_mca.get_reaction_ids(),
                      'get_model_details.reaction_names':        self._sbml_mca.get_reaction_names(),
                      'get_model_details.reaction_substrates':   self._sbml_mca.get_reaction_substrates(),
                      'get_model_details.reaction_products':     self._sbml_mca.get_reaction_products(),
                      'get_model_details.reaction_modifiers':    self._sbml_mca.get_reaction_modifiers(),
                      'get_model_details.reaction_k':            self._sbml_mca.get_reaction_kinetics(),
                      'get_model_details.rules':                 self._sbml_mca.get_rule_ids(),
                      'get_model_details.data': [] }
            time_course = self.get_time_course()
            result.update( time_course )
            #sys.stderr.write( str(result) )
            return self._update_db( result )
        except Exception, e:
            self._handle_error(e,'get_model_details')

    def get_time_course( self, end_time=2 ):
        """get simulation time course
           note: time couse does not contain constant species
        """
        try:
            [time,data] = self._sbml_mca.integrate( int(end_time), 100 )
            data = data.transpose()
            [time,data] = [time.tolist(),data.tolist()]
            result = { 'get_model_details.data': data,
                       'get_model_details.time': time,
                       'get_model_details.x_axis': self._sbml_mca.get_species_ids( include_constant=False ),
                       'get_model_details.x_axis_alt': self._sbml_mca.get_species_names( include_constant=False,
                                                                                         replace_empty_names_with_id=True ),
                       'get_model_details.y_axis': ['time'] }
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_model_details')
        return result


    def get_steady_state( self ):
        """get steady state and write result in DB
        """
        try:
            data = self._sbml_mca.get_steady_state().tolist()
            result = self._make_result_dict( 'get_steady_state',
                                             [data],
                                             x_axis=['ss'],
                                             y_axis=self._sbml_mca.get_species_ids(),
                                             y_axis_alt=self._sbml_mca.get_species_names( replace_empty_names_with_id=True ) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_steady_state')


    def get_parameter_elasticities( self ):
        try:
            data   = self._sbml_mca.get_parameter_elasticities( )
        except Exception, e:
            self._handle_error(e, 'get_parameter_elasticities')
        try:
            data_n = self._sbml_mca.get_parameter_elasticities( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_parameter_elasticities')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_parameter_elasticities',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis =     self._sbml_mca.get_reaction_ids(),
                                             x_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_parameter_ids(),
                                             y_axis_alt = self._sbml_mca.get_parameter_names(replace_empty_names_with_id=True) )
            #sys.stderr.write( str(result) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_parameter_elasticities')


    def get_metabolite_elasticities( self ):
        try:
            data   = self._sbml_mca.get_metabolite_elasticities( )
        except Exception, e:
            self._handle_error(e, 'get_metabolite_elasticities')
        try:
            data_n = self._sbml_mca.get_metabolite_elasticities( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_metabolite_elasticities')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_metabolite_elasticities',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis     = self._sbml_mca.get_reaction_ids(),
                                             x_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_species_ids(),
                                             y_axis_alt = self._sbml_mca.get_species_names(replace_empty_names_with_id=True) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_metabolite_elasticities')


    def get_flux_cc( self ):
        """ get flux control coefficients and write result in DB
        """
        try:
            data   = self._sbml_mca.get_flux_cc()
        except Exception, e:
            self._handle_error(e, 'get_fluxg_cc')
        try:
            data_n = self._sbml_mca.get_flux_cc( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_flux_cc')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_flux_cc',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis     = self._sbml_mca.get_reaction_ids(),
                                             x_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_reaction_ids(),
                                             y_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_flux_cc')


    def get_conc_cc( self ):
        """ get concentration control coefficients and write results in DB
        """
        try:
            data   = self._sbml_mca.get_conc_cc()
        except Exception, e:
            self._handle_error(e, 'get_conc_cc')
        try:
            data_n = self._sbml_mca.get_conc_cc( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_conc_cc')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_conc_cc',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis     = self._sbml_mca.get_species_ids(),
                                             x_axis_alt = self._sbml_mca.get_species_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_reaction_ids(),
                                             y_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_conc_cc')


    def get_flux_rc( self ):
        """get flux response coefficients and write results in DB
        """
        try:
            data   = self._sbml_mca.get_flux_resp()
        except Exception, e:
            self._handle_error(e, 'get_flux_rc')
        try:
            data_n = self._sbml_mca.get_flux_resp( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_flux_rc')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_flux_rc',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis     = self._sbml_mca.get_reaction_ids(),
                                             x_axis_alt = self._sbml_mca.get_reaction_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_parameter_ids(),
                                             y_axis_alt = self._sbml_mca.get_parameter_names(replace_empty_names_with_id=True) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_flux_rc')


    def get_conc_rc( self ):
        """get conc. response coefficients and write results in DB
        """
        try:
            data   = self._sbml_mca.get_conc_resp()
        except Exception, e:
            self._handle_error(e, 'get_conc_rc')
        try:
            data_n = self._sbml_mca.get_conc_resp( normalize='both' )
        except Exception, e:
            self._handle_error(e, 'get_conc_rc')
            data_n = numpy.zeros( data.shape )
        try:
            result = self._make_result_dict( 'get_conc_rc',
                                             data.tolist(),
                                             data_n.tolist(),
                                             x_axis     = self._sbml_mca.get_species_ids(),
                                             x_axis_alt = self._sbml_mca.get_species_names(replace_empty_names_with_id=True),
                                             y_axis     = self._sbml_mca.get_parameter_ids(),
                                             y_axis_alt = self._sbml_mca.get_parameter_names(replace_empty_names_with_id=True) )
            self._update_db( result )
        except Exception, e:
            self._handle_error(e, 'get_conc_rc')


    def get_time_varying_conc_rc( self, end_time=2, method='numerical' ):
        """ get time varying concentration response coefficients
        """
        if method=='numerical':
            errors=[]
            try:
                [time, tv_rc, tv_rc_norm, errors] = \
                        self._sbml_mca.get_time_varying_rc_web_if(float(end_time),
                                                                  what='conc',
                                                                  initial_cond_as_params=True):
            except Exception, e:
                time = tv_rc = tv_rc_norm = errors = None
                self._handle_error(e, 'get_time_varying_conc_rc')
            if errors!=[]:
                [ self._handle_error(e, 'get_time_varying_conc_rc') for e in errors ]
                #sys.stderr.write( str(tv_rc.tolist()) )
                #sys.stderr.write( str(tv_rc_norm.tolist()) )

            try:
                if tv_rc != None:
                    tv_rc = tv_rc.transpose()
                    tv_rc_norm = tv_rc_norm.transpose()
                    [time, tv_rc, tv_rc_norm] = [time.tolist(), tv_rc.tolist(), tv_rc_norm.tolist()]
                result = self._make_result_dict( 'get_time_varying_conc_rc',
                                                 tv_rc,
                                                 tv_rc_norm,
                                                 x_axis     = self._sbml_mca.get_species_ids(),
                                                 x_axis_alt = self._sbml_mca.get_species_names(replace_empty_names_with_id=True),
                                                 y_axis     = self._sbml_mca.get_parameter_ids() + self._sbml_mca.get_species_ids(),
                                                 y_axis_alt =
                                                     self._sbml_mca.get_parameter_names(replace_empty_names_with_id=True)
                                                   + self._sbml_mca.get_species_names(replace_empty_names_with_id=True),
                                                 time   = time )
                self._update_db( result )
            except Exception, e:
                self._handle_error(e, 'get_time_varying_conc_rc')

        else:
            raise Exception('Unknown method for rc computation.')


if __name__ == "__main__":
    import time, json, inspect
    host = 'localhost'
    if (os.uname()[1] == 'gofid'):
        port = 27017
    else:
        port = 3001
    api_obj = None
    api_executed = {} # take track of which method has been executed

    while api_executed=={} or False in api_executed.values():
        # while loop is running while api_obj is not initialized or not all public methods of api_obj have been executed
        line = sys.stdin.readline().strip()
        sys.stderr.write( 'incoming: ' + line )
        from_node = json.loads(line)
        cmd = from_node["method"]
        model_id = from_node["model"]
        arguments = ''
        if from_node.has_key("arguments"):
            arguments = from_node["arguments"]
        #sys.stderr.write( 'current model: ' + model_id )
        if cmd == "quit\n":
            break
        if not api_obj:
            api_obj = api( model_id, host, port  )
            for m_name, mclass in inspect.getmembers( api_obj, inspect.ismethod ): # get all methods that are not private
                if not m_name.startswith('_'):
                    api_executed[m_name] = False
        try:
            if model_id != api_obj._model_id:
                raise Exception('Wrong model ID')
            if arguments:
                getattr( api_obj, cmd)(**arguments)
            else:
                getattr( api_obj, cmd)()
            #print 'update return: ', json.dumps(ret)
        except Exception, e:
            sys.stderr.write( str(type(e)) + ': ' + e.message )
        #api_executed[cmd]=True
        time.sleep(.1)
        #sys.stdout.flush()

sys.stderr.write('While loop terminated')

