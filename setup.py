from distutils.core import setup

setup(
    name='sbml_mca',
    version='1.0',
    packages=['sbml_mca'],
    url='',
    license='MIT',
    author='floettmann',
    author_email='jannis.uhlendorf@hu-berlin.de',
    description='Metabolic Control Analysis in Python',
    scripts=["sbml_mca/sbml_mca.py"]
)
