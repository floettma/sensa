
Template.analysis.model = function() {
    return Session.get("selected_model") ? true : false;
};

Template.analysis.model_details = function() {
    return Session.get("selected_analysis") === 'get_model_details';
};

Template.analysis.time_dep = function() {
    return Session.get("selected_analysis") === 'get_time_varying_conc_rc';
};

Template.analysis.selected_model_name = function () {
    var selected_model = Session.get('selected_model');
    var selected_analysis = "get_model_details";
    var model_name = "";
    var fields =  {_id: 0};
    fields[selected_analysis] = 1;
    var model_details = Analysis.findOne({model_id: selected_model},
                                {fields: fields});
    if(typeof(model_details) !== "undefined"){
        var model_details = model_details[selected_analysis];
        var model_name = model_details.model_name;
    }
    if(model_name === "") {
        var model = Models.findOne(selected_model);
        if(typeof(model) !== "undefined")
            return model.name;
        else {
            return model_name;
        }
    }
    else {
        return model_name;
    }
};